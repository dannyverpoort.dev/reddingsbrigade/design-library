const colors = require('tailwindcss/colors')

module.exports = {
    important: true,
    mode: "jit",
    purge: {
        enabled: true,
        content: ["./src/**/*.tsx", "./stories/**/*.tsx"],
    },
    darkMode: "class",
    theme: {
        colors: {
            transparent: "transparent",
            white: "#ffffff",
            orange: {DEFAULT: '#ff5800'},
            red: {DEFAULT: '#d52a1e'},
            yellow: {DEFAULT: '#f9e300'},
            blue: {DEFAULT: '#0065bd', dark: '#003697'},
            cyan: {DEFAULT: '#009FDA'},
            error: colors.red,
            warning: colors.orange,
            success: colors.green,
        },
        fontSize: {
            xs: ["12px", "18px"],
            sm: ["14px", "20px"],
            md: ["16px", "24px"],
            lg: ["18px", "28px"],
            xl: ["20px", "30px"],
            '2xl': ["24px", "34px"],
            '3xl': ["28px", "38px"],
            '4xl': ["32px", "42px"],
            base: ["1em", "1"],

            h6: ["24px", "32px"],
            h5: ["30px", "38px"],
            h4: ["36px", "44px"],
            h3: ["48px", "60px"],
            h2: ["60px", "72px"],
            h1: ["72px", "90px"],
        },
        extend: {
            screens: {
                xs: "420px",
            },
            spacing: {
                4.5: "1.125rem",
                5.5: "1.375rem",
                6.5: "1.625rem",
                13: "3.25rem",
                15: "3.75rem",
                19: "4.75rem",
                19.5: "4.875rem",
                50: "12.5rem",
                58: "14.5rem",
                62: "15.5rem",
                70: "17.5rem",
            },
        },
        breakpointsInspector: {
            position: ["bottom", "right"],
        },
    },
    variants: {
        extend: {},
    },
    plugins: [
        require("@tailwindcss/forms"),
        require("tailwindcss-breakpoints-inscpector"),
        require('@tailwindcss/typography'),
    ],
};