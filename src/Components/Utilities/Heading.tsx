import React, {FC, HTMLAttributes} from 'react';

export interface Props extends HTMLAttributes<HTMLDivElement> {
    children?: string;
}

export const H1: FC<Props> = ({children, className = ''}) => {
    return <article className={'max-w-none prose'}>
        <h1 className={className}>{children}</h1>
    </article>;
};

export const H2: FC<Props> = ({children, className = ''}) => {
    return <article className={'max-w-none prose'}>
        <h2 className={className}>{children}</h2>
    </article>;
};

export const H3: FC<Props> = ({children, className = ''}) => {
    return <article className={'max-w-none prose'}>
        <h3 className={className}>{children}</h3>
    </article>;
};

export const H4: FC<Props> = ({children, className = ''}) => {
    return <article className={'max-w-none prose'}>
        <h4 className={className}>{children}</h4>
    </article>;
};

export const H5: FC<Props> = ({children, className = ''}) => {
    return <article className={'max-w-none prose'}>
        <h5 className={className}>{children}</h5>
    </article>;
};

export const H6: FC<Props> = ({children, className = ''}) => {
    return <article className={'max-w-none prose'}>
        <h6 className={className}>{children}</h6>
    </article>;
};

export const P: FC<Props> = ({children, className = ''}) => {
    return <article className={'max-w-none prose'}>
        <p className={className}>{children}</p>
    </article>;
};