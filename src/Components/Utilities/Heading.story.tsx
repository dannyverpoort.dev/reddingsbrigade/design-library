import React from 'react';
import { Meta, Story } from '@storybook/react';
import { H1, H3, H2, H4, H5, H6, P, Props } from "./Heading";

const meta: Meta = {
    title: 'Atoms/Typography',
    argTypes: {},
    parameters: {
        controls: { expanded: true },
    },
};

export default meta;

const Heading1Template: Story<Props> = args => <H1 {...args} />;
export const Heading_1 = Heading1Template.bind({});
Heading_1.args = {
    children: "The quick brown fox jumps over the lazy dog",
    className: 'text-orange'
};

const Heading2Template: Story<Props> = args => <H2 {...args} />;
export const Heading_2 = Heading2Template.bind({});
Heading_2.args = {
    children: "The quick brown fox jumps over the lazy dog",
    className: 'text-orange'
};

const Heading3Template: Story<Props> = args => <H3 {...args} />;
export const Heading_3 = Heading3Template.bind({});
Heading_3.args = {
    children: "The quick brown fox jumps over the lazy dog",
    className: 'text-orange'
};

const Heading4Template: Story<Props> = args => <H4 {...args} />;
export const Heading_4 = Heading4Template.bind({});
Heading_4.args = {
    children: "The quick brown fox jumps over the lazy dog",
    className: 'text-orange'
};

const Heading5Template: Story<Props> = args => <H5 {...args} />;
export const Heading_5 = Heading5Template.bind({});
Heading_5.args = {
    children: "The quick brown fox jumps over the lazy dog",
    className: 'text-orange'
};

const Heading6Template: Story<Props> = args => <H6 {...args} />;
export const Heading_6 = Heading6Template.bind({});
Heading_6.args = {
    children: "The quick brown fox jumps over the lazy dog",
    className: 'text-orange'
};

const HeadingPTemplate: Story<Props> = args => <P {...args} />;
export const Paragraph = HeadingPTemplate.bind({});
Paragraph.args = {
    children: `Morbi imperdiet nisl a purus tincidunt, nec euismod tortor luctus. Fusce lobortis, arcu quis venenatis sollicitudin, turpis dolor imperdiet dolor, id feugiat ante neque sit amet arcu. Ut feugiat eget sapien eget pellentesque. Praesent sollicitudin tincidunt pharetra. Quisque sit amet leo id tellus venenatis auctor a molestie mi. Suspendisse justo arcu, venenatis ac tempor vel, ullamcorper ut sapien. Vestibulum pharetra ultricies felis. Proin suscipit semper iaculis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Nunc luctus dolor maximus eros consequat posuere. Aliquam et volutpat enim. Vestibulum tincidunt faucibus mi vitae volutpat.`,
};