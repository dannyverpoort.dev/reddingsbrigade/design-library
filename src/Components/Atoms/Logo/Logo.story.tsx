import React from 'react';
import { Meta, Story } from '@storybook/react';
import { Logo, Props } from './Logo';

const meta: Meta = {
    title: 'Atoms/Logo',
    component: Logo,
    argTypes: {
        region: {
            control: {
                type: 'text',
            },
        },
        association: {
            control: {
                type: 'text',
            },
        },
        collapsed: {
            control: {
                type: 'boolean',
            },
        },
    },
    parameters: {
        controls: { expanded: true },
    },
};

export default meta;

const Template: Story<Props> = args => <Logo className={'w-1/4 text-orange'} {...args} />;

export const Basis = Template.bind({});
Basis.args = {};

export const Klein = Template.bind({});
Klein.args = {collapsed: true};

export const Regio = Template.bind({});
Regio.args = {region: 'Nederland', regionClassName:'text-blue-dark'};

export const Brigade = Template.bind({});
Brigade.args = {
    region: 'Overijssel-Twente',
    association: 'Enschede',
    regionClassName: 'text-blue'
};