import React from 'react';
import { Meta, Story } from '@storybook/react';
import { Header, Props } from './Header';
import {Logo} from "../../Atoms";

const meta: Meta = {
    title: 'Organisms/Header',
    component: Header,
    argTypes: {},
    parameters: {
        controls: { expanded: true },
    },
};

export default meta;

const Template: Story<Props> = args => <Header {...args} />;

export const Wit = Template.bind({});
Wit.args = {
    children: <Logo collapsed={false} className={'w-1/5 text-orange'} regionClassName={'text-blue'} associationClassName={'text-blue'} region={'Overijssel-Twente'} association={'Enschede'}/>,
    className: 'bg-white'
};

export const Oranje = Template.bind({});
Oranje.args = {
    children: <Logo collapsed={false} className={'w-1/5 text-white'} regionClassName={'text-blue-dark'} associationClassName={'text-white'} region={'Overijssel-Twente'} association={'Enschede'}/>
};