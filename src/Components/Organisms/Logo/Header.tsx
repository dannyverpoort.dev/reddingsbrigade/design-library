import React, {FC, HTMLAttributes, ReactChildren, ReactElement} from 'react';

export interface Props extends HTMLAttributes<HTMLDivElement> {
    children?: ReactChildren|ReactElement;
}

export const Header: FC<Props> = ({children, className= 'bg-orange'}) => {
    return <div className={className + ' px-3 py-3 shadow-lg '}>
        {children}
    </div>;
};